﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace practica_1.Models
{
    public class Game
    {
        #region Constructor
        public Game()
        {
            ActualTurn = "negro";
            MovesDone = new List<Disk>();
        }

        public Game(string initialTurn)
        {
            ActualTurn = initialTurn;
        }
        #endregion

        #region Methods
        public bool IsGameOver()
        {
            if (!GameBoard.Player1HasMovements && !GameBoard.Player2HasMovements)
                return true;
            int positionsAvailable = GameBoard.Columns * GameBoard.Rows;
            for (int i = 0; i < 8; i++)
                for (int j = 0; j < 8; j++)
                    if (GameBoard.GameBoard[i, j].Color != " ")
                        positionsAvailable--;
            if (positionsAvailable == 0)
                return true;
            return false;
        }

        public void UpdateTurn()
        {
            if (ActualTurn.Equals("blanco"))
            {
                ActualTurn = "negro";
            }
            else if (ActualTurn.Equals("negro"))
            {
                ActualTurn = "blanco";
            }
        }
        public void CountPoints()
        {
            int contBlack = 0, contWhite = 0;
            for (int i = 0; i < 8; i++)
                for (int j = 0; j < 8; j++)
                {
                    if (GameBoard.GameBoard[i, j].Color == "negro")
                        contBlack++;
                    else if (GameBoard.GameBoard[i, j].Color == "blanco")
                        contWhite++;
                }
            WhitePoints = contWhite;
            BlackPoints= contBlack;
        }

        public void SaveToDB()
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SaveGame";

                cmd.Parameters.Add("@jugador1", SqlDbType.VarChar).Value = Player1.Username;
                cmd.Parameters.Add("@jugador2", SqlDbType.VarChar).Value = Player2.Username;
                cmd.Parameters.Add("@puntosN", SqlDbType.Int).Value = WhitePoints;
                cmd.Parameters.Add("@puntosB", SqlDbType.Int).Value = BlackPoints;

                Winner = "ninguno";
                if (ReverseScore)
                {

                    if (WhitePoints < BlackPoints)
                        Winner = "Blanco";
                    else if (WhitePoints > BlackPoints)
                        Winner = "Negro";
                }
                else
                {
                    
                    if (WhitePoints > BlackPoints)
                        Winner = "Blanco";
                    else if (WhitePoints < BlackPoints)
                        Winner = "Negro";
                }
                
                cmd.Parameters.Add("@ganador", SqlDbType.VarChar).Value = Winner;
                cmd.Connection = conn;
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public void AddMovementToList(int col, int row)
        {
            MovesDone.Add(new Disk()
            {
                Color = ActualTurn,
                Row = (row + 1).ToString(),
                Column = GameBoard.LetterDictionary[col]
            });
        }
        #endregion

        #region Properties
        public Timer WhiteTimer { get; set; }
        public Timer BlackTimer { get; set; }
        public Board GameBoard { get; set; }
        public Account Player1 { get; set; }
        public Account Player2 { get; set; }
        public List<Disk> MovesDone { get; set; }
        public string ActualTurn { get; set; }
        public int BlackPoints { get; set; }
        public int WhitePoints { get; set; }
        public string Winner { get; set; }
        public bool CustomOpening { get; set; }
        public bool ReverseScore { get; set; }
        #endregion
    }
}