﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace practica_1.Models
{
    public class Disk
    {
        public string Color { get; set; }
        public string Column { get; set; }
        public string Row { get; set; }
    }
}