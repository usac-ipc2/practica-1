﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace practica_1.Models
{
    public class Timer
    {
        public DateTime InitialTime { get; set; }
        public DateTime FinalTime { get; set; }
        public DateTime ElapsedTime { get; set; }
        public int ElapsedTimeInt { get; set; }

        public Timer()
        {
            ElapsedTime = new DateTime(0);
        }

        public void SetInitialTime()
        {
            InitialTime = DateTime.Now;
        }
        public  void SetFinalTime()
        {
            FinalTime = DateTime.Now;
            ElapsedTime = ElapsedTime.Add(FinalTime.Subtract(InitialTime));
        }
    }
}