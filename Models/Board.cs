﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace practica_1.Models
{
    public class Board
    {

        #region Contructor
        public Board(int columns, int rows, string gameType, bool multiplayer)
        {
            LetterDictionary = new Dictionary<int, string>();
            LetterDictionary.Add(0, "A");
            LetterDictionary.Add(1, "B");
            LetterDictionary.Add(2, "C");
            LetterDictionary.Add(3, "D");
            LetterDictionary.Add(4, "E");
            LetterDictionary.Add(5, "F");
            LetterDictionary.Add(6, "G");
            LetterDictionary.Add(7, "H");
            GameBoard = new Disk[rows, columns];
            GameBoardString = new string[rows, columns];
            Columns = columns;
            Rows = rows;
            for (int i = 0; i < Rows; i++)
                for (int j = 0; j < Columns; j++)
                {
                    GameBoard[i, j] = new Disk
                    {
                        Color = " ",
                        Column = $"{j}",
                        Row = LetterDictionary[i]
                    };
                    GameBoardString[i, j] = " ";
                }
            if (gameType == "classic")
            {
                GameBoard[3, 3].Color = "blanco";
                GameBoard[3, 4].Color = "negro";
                GameBoard[4, 3].Color = "negro";
                GameBoard[4, 4].Color = "blanco";
                GameBoardString[3, 3] = "blanco";
                GameBoardString[3, 4] = "negro";
                GameBoardString[4, 3] = "negro";
                GameBoardString[4, 4] = "blanco";
            }
            else if (gameType == "custom_opening")
            {
                CustomOpening = true;
                CustomOpeningPieces = 0;
            }
            Player1HasMovements = true;
            Player2HasMovements = true;
        }
        #endregion

        #region Methods
        public void DisplayValidMoves(string color)
        {
            //var board = new string[Rows, Columns];

            var board = CalculateValidMoves(color);
                int availableMovements = 0;
                for (int i = 0; i < Rows; i++)
                {
                    for (int j = 0; j < Columns; j++)
                    {
                        if (!string.IsNullOrEmpty(board[i, j]))
                        {
                            GameBoardString[i, j] = board[i, j];
                            GameBoard[i, j].Color = board[i, j];
                            availableMovements++;
                        }
                    }
                }
                if (availableMovements == 0)
                    Player1HasMovements = false;
                else
                    Player1HasMovements = true;
        }
        public string[,] DisplayValidMovesCustomOpening(string color)
        {
            var board = new string[2, 2];
            return board;
        }
        public string[,] CalculateValidMoves(string color)
        {
            string[,] validMoves = new string[Rows, Columns];
            if (CustomOpening && CustomOpeningPieces < 4)
            {
                if (GameBoardString[(Rows / 2) - 1, (Columns / 2) - 1] == " ")
                    validMoves[(Rows / 2) - 1, (Columns / 2) - 1] = "posible";
                if (GameBoardString[(Rows / 2) - 1, (Columns / 2)] == " ")
                    validMoves[(Rows / 2) - 1, (Columns / 2)] = "posible";
                if (GameBoardString[(Rows / 2), (Columns / 2) - 1] == " ")
                    validMoves[(Rows / 2), (Columns / 2) - 1] = "posible";
                if (GameBoardString[(Rows / 2), (Columns / 2)] == " ")
                    validMoves[(Rows / 2), (Columns / 2)] = "posible";
            }
            else
            {
                for (int row = 0; row < Rows; row++)
                {
                    for (int col = 0; col < Columns; col++)
                    {
                        if (GameBoardString[row, col] == " ")
                        {
                            if (col == 3 && row == 3)
                                Console.WriteLine("VALIDACION AQUI");
                            var ari = ValidMove(color, -1, -1, row, col);
                            var ar = ValidMove(color, -1, 0, row, col);
                            var ard = ValidMove(color, -1, 1, row, col);

                            var i = ValidMove(color, 0, -1, row, col);
                            var d = ValidMove(color, 0, 1, row, col);

                            var abi = ValidMove(color, 1, -1, row, col);
                            var ab = ValidMove(color, 1, 0, row, col);
                            var abd = ValidMove(color, 1, 1, row, col);
                            if (ari || ar || ard || i || d || abi || ab || abd)
                            {
                                validMoves[row, col] = "posible";
                            }
                        }
                    }
                }
            }
            return validMoves;
        }

        /*
            Verifica si al sumar un cambio de posicion en la columna o la fila, termina en el color de él mismo
            para de esta forma validar el movimiento
         */
        public bool ValidMove(string color, int dr, int dc, int row, int col)
        {
            string other_color = "";
            if (color.Equals("blanco"))
                other_color = "negro";
            else if (color.Equals("negro"))
                other_color = "blanco";
            else
                return false;
            if ((row + dr < 0) || (row + dr > Rows - 1))
                return false;
            if ((col + dc < 0) || (col + dc > Columns - 1))
                return false;
            if (this.GameBoardString[row + dr, col + dc] != other_color)
                return false;
            if ((row + dr + dr < 0) || (row + dr + dr > Rows-1))
                return false;
            if ((col + dc + dc < 0) || (col + dc + dc > Rows-1))
                return false;
            return CheckEndOfLine(color, dr, dc, row + dr + dr, col + dc + dc);
        }

        /*
            Verifica si al final de la linea exisite un color del mismo de la ficha
            o en su defecto, si existe el color tras una serie de fichas de color opuesto
         */

        public bool CheckEndOfLine(string color, int dr, int dc, int row, int col)
        {
            if ((row + dr < 0) || (row + dr > Rows))
                return false;
            if ((col + dc < 0) || (col + dc > Columns))
                return false;
            if (GameBoardString[row, col].Equals(color))
                return true;
            if (this.GameBoardString[row, col] == " ")
                return false;
            return CheckEndOfLine(color, dr, dc, row + dr, col + dc);
        }

        public void FlipCoins(string color, int row, int col)
        {
            FlipLine(color, -1, -1, row, col);
            FlipLine(color, -1, 0, row, col);
            FlipLine(color, -1, 1, row, col);

            FlipLine(color, 0, -1, row, col);
            FlipLine(color, 0, 1, row, col);

            FlipLine(color, 1, -1, row, col);
            FlipLine(color, 1, 0, row, col);
            FlipLine(color, 1, 1, row, col);
        }

        public bool FlipLine(string color, int dr, int dc, int row, int col)
        {
            if ((row + dr < 0) || (row + dr > 7))
                return false;
            if ((col + dc < 0) || (col + dc > 7))
                return false;
            if (this.GameBoardString[row + dr, col + dc] == " ")
                return false;
            if (this.GameBoardString[row + dr, col + dc] == color)
                return true;
            else
            {
                if (FlipLine(color, dr, dc, row + dr, col + dc))
                {
                    this.GameBoard[row + dr, col + dc].Color = color;
                    this.GameBoardString[row + dr, col + dc] = color;
                    return true;
                }
                else
                    return false;
            }
        }

        public void CleanBoard()
        {
            for (int i = 0; i < 8; i++)
                for (int j = 0; j < 8; j++)
                    if (GameBoard[i, j].Color.Equals("posible"))
                    {
                        GameBoard[i, j].Color = " ";
                        GameBoardString[i, j] = " ";
                    }
        }

        public void CPUMovement(string color)
        {
            var posibilities = new List<string>();

            var validMoves = CalculateValidMoves(color);
            for (int i = 0; i < 8; i++)
                for (int j = 0; j < 8; j++)
                    if (!string.IsNullOrEmpty(validMoves[i, j]))
                        posibilities.Add($"{i},{j}");

            if (posibilities.Count>0)
            {
                Player2HasMovements = true;
                var random = new Random();
                int num = random.Next(posibilities.Count);
                var tempPos = posibilities[num].Split(',');

                GameBoard[Convert.ToInt32(tempPos[0]), Convert.ToInt32(tempPos[1])].Color = color;
                GameBoardString[Convert.ToInt32(tempPos[0]), Convert.ToInt32(tempPos[1])] = color;
                FlipCoins(color, Convert.ToInt32(tempPos[0]), Convert.ToInt32(tempPos[1]));
            }
            else
            {
                Console.WriteLine("EL CPU YA NO TIENE MAS MOVIMENTOS");
                Player2HasMovements = false;
            }
        }
        #endregion

        #region Properties
        public bool Player1HasMovements { get; set; }
        public bool Player2HasMovements { get; set; }
        public string[,] GameBoardString { get; set; }
        public Disk[,] GameBoard { get; set; }
        public int Columns { get; set; }
        public int Rows { get; set; }
        public Dictionary<int, string> LetterDictionary { get; set; }
        public int CustomOpeningPieces { get; set; }
        public bool CustomOpening { get; set; }
        #endregion
    }
}