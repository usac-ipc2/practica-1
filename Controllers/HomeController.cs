﻿using practica_1.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace practica_1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if(Session["actualUser"] == null)
            {
                return RedirectToAction("Login", "Account");
            }
            if (Session["CPUGame"] == null)
            {
                var tempUser = (Account)Session["actualUser"];
                var initialGame = new Game
                {
                    BlackPoints = 2,
                    WhitePoints = 2,
                    GameBoard = new Board(8, 8, "classic", false),
                    Player1 = tempUser,
                    Player2 = new Account { Username = "CPU" },
                    ActualTurn = "negro",
                    BlackTimer = new Models.Timer(),
                    WhiteTimer = new Models.Timer(),
                };
                initialGame.BlackTimer.SetInitialTime();
                initialGame.GameBoard.DisplayValidMoves(initialGame.ActualTurn);
                Session["CPUGame"] = initialGame;
            }
            Thread t = new Thread(new ThreadStart(Chronometer));
            //t.Start();
            return View(Session["CPUGame"]);
        }
        private void Chronometer()
        {
            var actualTurn = Session["actualTurn"].ToString();
            var elapsedTimeBlack = Convert.ToDateTime(Session["blackTime"].ToString());
            var elapsedTimeWhite = Convert.ToDateTime(Session["whiteTime"].ToString());
            var initialTime = DateTime.Now;
            int cont = 0;
            while(actualTurn == "negro")
            {
                var finalTime = DateTime.Now;
                var elapsedTime = finalTime - initialTime;
                var elapsedTimeInt = elapsedTime;
                var tempTime = elapsedTimeBlack + elapsedTime;
                var tempSess = Session["blackTime"];
                if (Session["blackTime"] != null)
                    Session["blackTime"] = tempTime ;
                Thread.Sleep(5000);
                TempData["reloadingMessage"] = "reload";
                cont++;
                //UpdateView(tempTime.ToString());
            }
            while (actualTurn == "blanco")
            {
                var finalTime = DateTime.Now;
                var elapsedTime = finalTime - initialTime;
                var elapsedTimeInt = elapsedTime;
                Session["whiteTime"] = elapsedTimeWhite + elapsedTime;
                TempData["reloadingMessage"] = "reload";
                //UpdateView(elapsedTime.ToString());
            }
        }
        /*Used to update the chronometer view */
        public ActionResult UpdateView(string elapsedTime)
        {
            //return PartialView("_HomeBoard");
            return PartialView("_HomeBoardTimer", elapsedTime);
        }
        [HttpPost]
        public ActionResult NewGame(string mode1, string mode2)
        {
            var tempUser = (Account)Session["actualUser"];
            var initialGame = new Game
            {
                BlackPoints = 2,
                WhitePoints = 2,
                GameBoard = new Board(8, 8, "classic", false),
                Player1 = tempUser,
                Player2 = new Account { Username = "CPU" },
                ActualTurn = "negro",
            };
            if (!string.IsNullOrEmpty(mode1))
                if (mode1 == "custom_opening")
                    initialGame = new Game
                    {
                        BlackPoints = 2,
                        WhitePoints = 2,
                        GameBoard = new Board(8, 8, "custom_opening", false),
                        Player1 = tempUser,
                        Player2 = new Account { Username = "CPU" },
                        ActualTurn = "negro",
                    };
            if (!string.IsNullOrEmpty(mode2))
                if (mode2 == "reverse_score")
                    initialGame.ReverseScore = true;
            initialGame.GameBoard.DisplayValidMoves(initialGame.ActualTurn);
            Session["CPUGame"] = initialGame;
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0)
                try
                {
                    var gameBoard = (string[,]) Session["gameBoard"];
                    //string path = Path.Combine(Server.MapPath("~/Games"),
                    //                           Path.GetFileName(file.FileName));
                    //file.SaveAs(path);
                    XmlDocument xmlDocument = new XmlDocument();
                    List<Disk> movesDone = new List<Disk>();
                    xmlDocument.Load(file.InputStream);
                    XmlNodeList diskNodes = xmlDocument.SelectNodes("tablero/ficha");
                    foreach (XmlNode disk in diskNodes)
                    {
                        int Column = 0;
                        var diskItem = new Disk()
                        {
                            Color = disk["color"].InnerText,
                            Column = disk["columna"].InnerText,
                            Row = disk["fila"].InnerText,
                        };

                        switch (diskItem.Column)
                        {
                            case "A":
                                Column = 0;
                                break;
                            case "B":
                                Column = 1;
                                break;
                            case "C":
                                Column = 2;
                                break;
                            case "D":
                                Column = 3;
                                break;
                            case "E":
                                Column = 4;
                                break;
                            case "F":
                                Column = 5;
                                break;
                            case "G":
                                Column = 6;
                                break;
                            case "H":
                                Column = 7;
                                break;
                            default:
                                break;
                        }
                        int Row = int.Parse(disk["fila"].InnerText)-1;
                        gameBoard[Row, Column] = diskItem.Color;
                        movesDone.Add(diskItem);
                    }

                    foreach (var move in movesDone)
                    {
                        int colNumber = 0;
                        int rowNumber = Convert.ToInt32(move.Row) - 1;
                        switch (move.Column)
                        {
                            case "A":
                                colNumber = 0;
                                break;
                            case "B":
                                colNumber = 1;
                                break;
                            case "C":
                                colNumber = 2;
                                break;
                            case "D":
                                colNumber = 3;
                                break;
                            case "E":
                                colNumber = 4;
                                break;
                            case "F":
                                colNumber = 5;
                                break;
                            case "G":
                                colNumber = 6;
                                break;
                            case "H":
                                colNumber = 7;
                                break;
                            default:
                                break;
                        }
                        var ari = TokenPositionValid(colNumber-1, rowNumber - 1, gameBoard);
                        var ar = TokenPositionValid(colNumber, rowNumber - 1, gameBoard);
                        var ard = TokenPositionValid(colNumber + 1, rowNumber - 1, gameBoard);

                        var i = TokenPositionValid(colNumber - 1, rowNumber, gameBoard);
                        var d = TokenPositionValid(colNumber + 1, rowNumber, gameBoard);

                        var abi = TokenPositionValid(colNumber - 1, rowNumber + 1, gameBoard);
                        var ab = TokenPositionValid(colNumber, rowNumber + 1, gameBoard);
                        var abd = TokenPositionValid(colNumber + 1, rowNumber + 1, gameBoard);
                        if(!ari && !ar && !ard && !abi && !ab && !abd && !i && !d)
                        {
                            gameBoard[rowNumber, colNumber] = " ";
                        }
                    }

                    XmlNode nextTurn = xmlDocument.SelectSingleNode("tablero/siguienteTiro");

                    string actualTurn = nextTurn["color"].InnerText;
                    actualTurn = actualTurn.Replace(" ", string.Empty);
                    Session["actualTurn"] = actualTurn;
                    ViewBag.ActualTurn = Session["actualTurn"].ToString();
                    Session["gameBoard"] = gameBoard;
                    Session["movesDone"] = movesDone;
                    ViewBag.Message = "Archivo cargado exitosamente";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    ViewBag.Message = "ERROR:" + ex.Message.ToString();
                    return RedirectToAction("Index");
                }
            else
            {
                ViewBag.Message = "You have not specified a file.";
                return RedirectToAction("Index");
            }
        }

        public ActionResult UpdateBoard(int row, int column)
        {
            var game = (Game) Session["CPUGame"];
            //game.BlackTimer.SetFinalTime();
            game.GameBoard.GameBoard[row, column].Color = game.ActualTurn;
            game.GameBoard.GameBoardString[row, column] = game.ActualTurn;
            if (game.GameBoard.CustomOpening && game.GameBoard.CustomOpeningPieces < 4)
                game.GameBoard.CustomOpeningPieces++;
            game.AddMovementToList(column, row);
            game.GameBoard.CleanBoard();
            game.GameBoard.FlipCoins(game.ActualTurn, row, column);
            game.UpdateTurn();
            //game.WhiteTimer.SetInitialTime();
            game.GameBoard.CPUMovement(game.ActualTurn);
            if (game.GameBoard.CustomOpening && game.GameBoard.CustomOpeningPieces < 4)
                game.GameBoard.CustomOpeningPieces++;
            //game.WhiteTimer.SetFinalTime();
            game.UpdateTurn();
            game.GameBoard.DisplayValidMoves(game.ActualTurn);
            game.CountPoints();

            if (game.IsGameOver())
            {
                if(game.ReverseScore)
                {
                    if (game.WhitePoints < game.BlackPoints)
                        TempData["message"] = "El ganador es Blanco";
                    else if (game.BlackPoints < game.WhitePoints)
                        TempData["message"] = "El ganador es  Negro";
                    else
                        TempData["message"] = "El resultado final es empate";
                }
                else
                {
                    if (game.WhitePoints > game.BlackPoints)
                        TempData["message"] = "El ganador es Blanco";
                    else if (game.BlackPoints > game.WhitePoints)
                        TempData["message"] = "El ganador es  Negro";
                    else
                        TempData["message"] = "El resultado final es empate";
                }
                
                game.SaveToDB();
                return RedirectToAction("Index", "Home");
            }
            return RedirectToAction("Index");
        }
        private bool HasValidMoves(string[,] board)
        {
            int cont = 0;
            for (int i = 0; i < 8; i++)
                for (int j = 0; j < 8; j++)
                    if(!string.IsNullOrEmpty(board[i,j]))
                            cont++;
            if (cont > 0)
                return true;
            else
                return false;
        }   

        private bool TokenPositionValid(int col, int row, string[,] board)
        {
            if (col < 0 || col > 7)
                return false;
            else if (row < 0 || row > 7)
                return false;
            else if (board[row, col] == " ")
                return false;
            else
                return true;
        }

        public ActionResult Download()
        {
            var gameBoard = (string[,])Session["gameBoard"];
            var movesDone = (List<Disk>)Session["movesDone"];
            string actualTurn = Session["actualTurn"].ToString();
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.IndentChars = ("  ");
            settings.CloseOutput = true;
            settings.OmitXmlDeclaration = true;
            string date = DateTime.Today.ToString();
            date = date.Substring(0, 9);
            date = date.Replace("/", "-");
            var assemblyPath = new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath;
            var directoryPath = Path.GetDirectoryName(assemblyPath);
            var filePath = Path.Combine(directoryPath, "partida-" + date + ".xml");
            using (XmlWriter writer = XmlWriter.Create( filePath, settings))
            {
                writer.WriteStartElement("tablero");
                foreach (var move in movesDone)
                {
                    writer.WriteStartElement("ficha");
                    writer.WriteElementString("color", move.Color);
                    writer.WriteElementString("columna", move.Column);
                    writer.WriteElementString("fila", move.Row);
                    writer.WriteEndElement();
                }
                writer.WriteStartElement("siguienteTiro");
                writer.WriteElementString("color", actualTurn);
                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.Flush();
                writer.Close();
            }
            return RedirectToAction("Index");
        }
    }
}