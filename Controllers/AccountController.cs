﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using practica_1.Models;

namespace practica_1.Controllers
{
    public class AccountController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "P1Login";
                    cmd.Parameters.Add("@passWord", SqlDbType.VarChar).Value = model.Password;
                    cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = model.Email;
                    cmd.Connection = conn;
                    conn.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    if (dr.Read())
                    {
                        var response = new Account();
                        response.FirstName = (string)dr["FirstName"];
                        response.LastName = (string)dr["LastName"];
                        response.Country = (string)dr["Country"];
                        response.BirthDate = (DateTime)dr["BirthDate"];
                        response.Id = (int)dr["IdAccount"];
                        response.Email = (string)dr["Email"];
                        response.Username = (string)dr["Username"];
                        Session["actualUser"] = response;
                    }
                }
                // This doesn't count login failures towards account lockout
                // To enable password failures to trigger account lockout, change to shouldLockout: true
                return RedirectToAction("Index", "Home");
            }
            return View(model);
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "P1Register";
                    cmd.Parameters.Add("@password", SqlDbType.VarChar).Value = model.Password;
                    cmd.Parameters.Add("@username", SqlDbType.VarChar).Value = model.Username;
                    cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = model.Email;
                    cmd.Parameters.Add("@firstName", SqlDbType.VarChar).Value = model.FirstName;
                    cmd.Parameters.Add("@lastName", SqlDbType.VarChar).Value = model.LastName;
                    cmd.Parameters.Add("@birthDate", SqlDbType.VarChar).Value = model.BirthDate;
                    cmd.Parameters.Add("@country", SqlDbType.VarChar).Value = "Guatemala";
                    cmd.Connection = conn;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
                return RedirectToAction("Index", "Home"); ;
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }
    }
}