﻿using practica_1.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace practica_1.Controllers
{
    public class OthXtremeController : Controller
    {
        public ActionResult Index()
        {
            if (Session["actualUser"] == null)
            {
                return RedirectToAction("Login", "Account");
            }
            if (Session["MultiplayerGame"] == null)
            {
                var tempUser = (Account)Session["actualUser"];
                var initialGame = new Game
                {
                    BlackPoints = 2,
                    WhitePoints = 2,
                    GameBoard = new Board(8, 8, "classic", false),
                    Player1 = tempUser,
                    ActualTurn = "negro",
                };
                //initialGame.GameBoard.DisplayValidMoves(initialGame.ActualTurn);
                Session["MultiplayerGame"] = initialGame;
            }
            //t.Start();
            return View(Session["MultiplayerGame"]);
        }

        public ActionResult UpdateBoard(int row, int column)
        {
            var game = (Game)Session["MultiplayerGame"];
            game.GameBoard.GameBoard[row, column].Color = game.ActualTurn;
            game.GameBoard.GameBoardString[row, column] = game.ActualTurn;
            if (game.GameBoard.CustomOpening && game.GameBoard.CustomOpeningPieces < 4)
                game.GameBoard.CustomOpeningPieces++;
            game.AddMovementToList(column, row);
            game.GameBoard.CleanBoard();
            game.GameBoard.FlipCoins(game.ActualTurn, row, column);
            game.UpdateTurn();
            game.GameBoard.DisplayValidMoves(game.ActualTurn);
            game.CountPoints();

            if (game.IsGameOver())
            {
                if (game.ReverseScore)
                {
                    if (game.WhitePoints < game.BlackPoints)
                        TempData["message"] = "El ganador es Blanco";
                    else if (game.BlackPoints < game.WhitePoints)
                        TempData["message"] = "El ganador es  Negro";
                    else
                        TempData["message"] = "El resultado final es empate";
                }
                else
                {
                    if (game.WhitePoints > game.BlackPoints)
                        TempData["message"] = "El ganador es Blanco";
                    else if (game.BlackPoints > game.WhitePoints)
                        TempData["message"] = "El ganador es  Negro";
                    else
                        TempData["message"] = "El resultado final es empate";
                }

                game.SaveToDB();
                return RedirectToAction("Index", "Home");
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult NewGame(string username, string mode1, string mode2)
        {
            if (string.IsNullOrEmpty(username))
            {
                return RedirectToAction("Index");
            }
            var tempUser = (Account)Session["actualUser"];
            var initialGame = new Game
            {
                BlackPoints = 2,
                WhitePoints = 2,
                GameBoard = new Board(8, 8, "classic", false),
                Player1 = tempUser,
                Player2 = new Account { Username = username },
                ActualTurn = "negro",
            };
            if (!string.IsNullOrEmpty(mode1))
                if (mode1 == "custom_opening")
                    initialGame = new Game
                    {
                        BlackPoints = 2,
                        WhitePoints = 2,
                        GameBoard = new Board(8, 8, "custom_opening", false),
                        Player1 = tempUser,
                        Player2 = new Account { Username = username },
                        ActualTurn = "negro",
                    };
            if (!string.IsNullOrEmpty(mode2))
                if (mode2 == "reverse_score")
                    initialGame.ReverseScore = true;
            initialGame.GameBoard.DisplayValidMoves(initialGame.ActualTurn);
            Session["MultiplayerGame"] = initialGame;
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0)
                try
                {
                    var gameBoard = (string[,])Session["gameBoard"];
                    //string path = Path.Combine(Server.MapPath("~/Games"),
                    //                           Path.GetFileName(file.FileName));
                    //file.SaveAs(path);
                    XmlDocument xmlDocument = new XmlDocument();
                    List<Disk> movesDone = new List<Disk>();
                    xmlDocument.Load(file.InputStream);
                    XmlNodeList diskNodes = xmlDocument.SelectNodes("tablero/ficha");
                    foreach (XmlNode disk in diskNodes)
                    {
                        int Column = 0;
                        var diskItem = new Disk()
                        {
                            Color = disk["color"].InnerText,
                            Column = disk["columna"].InnerText,
                            Row = disk["fila"].InnerText,
                        };

                        switch (diskItem.Column)
                        {
                            case "A":
                                Column = 0;
                                break;
                            case "B":
                                Column = 1;
                                break;
                            case "C":
                                Column = 2;
                                break;
                            case "D":
                                Column = 3;
                                break;
                            case "E":
                                Column = 4;
                                break;
                            case "F":
                                Column = 5;
                                break;
                            case "G":
                                Column = 6;
                                break;
                            case "H":
                                Column = 7;
                                break;
                            default:
                                break;
                        }
                        int Row = int.Parse(disk["fila"].InnerText) - 1;
                        gameBoard[Row, Column] = diskItem.Color;
                        movesDone.Add(diskItem);
                    }

                    foreach (var move in movesDone)
                    {
                        int colNumber = 0;
                        int rowNumber = Convert.ToInt32(move.Row) - 1;
                        switch (move.Column)
                        {
                            case "A":
                                colNumber = 0;
                                break;
                            case "B":
                                colNumber = 1;
                                break;
                            case "C":
                                colNumber = 2;
                                break;
                            case "D":
                                colNumber = 3;
                                break;
                            case "E":
                                colNumber = 4;
                                break;
                            case "F":
                                colNumber = 5;
                                break;
                            case "G":
                                colNumber = 6;
                                break;
                            case "H":
                                colNumber = 7;
                                break;
                            default:
                                break;
                        }
                        var ari = TokenPositionValid(colNumber - 1, rowNumber - 1, gameBoard);
                        var ar = TokenPositionValid(colNumber, rowNumber - 1, gameBoard);
                        var ard = TokenPositionValid(colNumber + 1, rowNumber - 1, gameBoard);

                        var i = TokenPositionValid(colNumber - 1, rowNumber, gameBoard);
                        var d = TokenPositionValid(colNumber + 1, rowNumber, gameBoard);

                        var abi = TokenPositionValid(colNumber - 1, rowNumber + 1, gameBoard);
                        var ab = TokenPositionValid(colNumber, rowNumber + 1, gameBoard);
                        var abd = TokenPositionValid(colNumber + 1, rowNumber + 1, gameBoard);
                        if (!ari && !ar && !ard && !abi && !ab && !abd && !i && !d)
                        {
                            gameBoard[rowNumber, colNumber] = " ";
                        }
                    }
                    XmlNode nextTurn = xmlDocument.SelectSingleNode("tablero/siguienteTiro");

                    string actualTurn = nextTurn["color"].InnerText;
                    actualTurn = actualTurn.Replace(" ", string.Empty);
                    Session["actualTurn"] = actualTurn;
                    ViewBag.ActualTurn = Session["actualTurn"].ToString();

                    Session["movesDone"] = movesDone;
                    ViewBag.Message = "Archivo cargado exitosamente";
                    var validMoves = CalculateValidMoves(actualTurn, gameBoard);
                    for (int i = 0; i < 8; i++)
                        for (int j = 0; j < 8; j++)
                            if (!string.IsNullOrEmpty(validMoves[i, j]))
                                gameBoard[i, j] = validMoves[i, j];
                    Session["gameBoard"] = gameBoard;
                    return RedirectToAction("Index", "Home");
                }
                catch (Exception ex)
                {
                    ViewBag.Message = "ERROR:" + ex.Message.ToString();
                    return RedirectToAction("Index");
                }
            else
            {
                ViewBag.Message = "You have not specified a file.";
                return RedirectToAction("Index");
            }
        }
        public ActionResult Download()
        {
            var gameBoard = (string[,])Session["gameBoard"];
            var movesDone = (List<Disk>)Session["movesDone"];
            string actualTurn = Session["actualTurn"].ToString();
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.IndentChars = ("  ");
            settings.CloseOutput = true;
            settings.OmitXmlDeclaration = true;
            string date = DateTime.Today.ToString();
            date = date.Substring(0, 9);
            date = date.Replace("/", "-");
            var assemblyPath = new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath;
            var directoryPath = Path.GetDirectoryName(assemblyPath);
            var filePath = Path.Combine(directoryPath, "partida-" + date + ".xml");
            using (XmlWriter writer = XmlWriter.Create(filePath, settings))
            {
                writer.WriteStartElement("tablero");
                foreach (var move in movesDone)
                {
                    writer.WriteStartElement("ficha");
                    writer.WriteElementString("color", move.Color);
                    writer.WriteElementString("columna", move.Column);
                    writer.WriteElementString("fila", move.Row);
                    writer.WriteEndElement();
                }
                writer.WriteStartElement("siguienteTiro");
                writer.WriteElementString("color", actualTurn);
                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.Flush();
                writer.Close();
            }
            return RedirectToAction("Index");
        }
        private bool IsGameOver()
        {
            var board = (string[,])Session["gameBoardM"];
            var validMovesB = CalculateValidMoves("negro", board);
            var validMovesW = CalculateValidMoves("blanco", board);
            if (!HasValidMoves(validMovesB) && !HasValidMoves(validMovesW))
                return true;
            else
            {
                //Contador de las casillas en blanco 
                int cont = 0;
                for (int i = 0; i < 8; i++)
                    for (int j = 0; j < 8; j++)
                        if (board[i, j] == " ")
                            cont++;
                if (cont > 0)
                    return false;
                else
                    return true;
            }
        }

        private bool HasValidMoves(string[,] board)
        {
            int cont = 0;
            for (int i = 0; i < 8; i++)
                for (int j = 0; j < 8; j++)
                    if (!string.IsNullOrEmpty(board[i, j]))
                        cont++;
            if (cont > 0)
                return true;
            else
                return false;
        }

        private string[,] CalculateValidMoves(string color, string[,] board)
        {
            string[,] validMoves = new string[8, 8];
            for (int row = 0; row < 8; row++)
            {
                for (int col = 0; col < 8; col++)
                {
                    if (board[row, col] == " ")
                    {
                        var ari = ValidMove(color, -1, -1, row, col, board);
                        var ar = ValidMove(color, -1, 0, row, col, board);
                        var ard = ValidMove(color, -1, 1, row, col, board);

                        var i = ValidMove(color, 0, -1, row, col, board);
                        var d = ValidMove(color, 0, 1, row, col, board);

                        var abi = ValidMove(color, 1, -1, row, col, board);
                        var ab = ValidMove(color, 1, 0, row, col, board);
                        var abd = ValidMove(color, 1, 1, row, col, board);
                        if (ari || ar || ard || i || d || abi || ab || abd)
                        {
                            validMoves[row, col] = "posible";
                        }
                    }
                }
            }
            return validMoves;
        }

        /*
            Verifica si al sumar un cambio de posicion en la columna o la fila, termina en el color de él mismo
            para de esta forma validar el movimiento
         */
        private bool ValidMove(string color, int dy, int dx, int row, int col, string[,] board)
        {
            string other_color = "";
            if (color.Equals("blanco"))
                other_color = "negro";
            else if (color.Equals("negro"))
                other_color = "blanco";
            if ((row + dy < 0) || (row + dy > 7))
                return false;
            if ((col + dx < 0) || (col + dx > 7))
                return false;
            if (board[row + dy, col + dx] != other_color)
                return false;
            if ((row + dy + dy < 0) || (row + dy + dy > 7))
                return false;
            if ((col + dx + dx < 0) || (col + dx + dx > 7))
                return false;
            return CheckEndOfLine(color, dy, dx, row + dy + dy, col + dx + dx, board);
        }

        /*
            Verifica si al final de la linea exisite un color del mismo de la ficha
            o en su defecto, si existe el color tras una serie de fichas de color opuesto
         */

        private bool CheckEndOfLine(string color, int dy, int dx, int row, int col, string[,] board)
        {
            if (board[row, col].Equals(color))
                return true;
            if (board[row, col] != " ")
                return false;
            if ((row + dy < 0) || (row + dy > 7))
                return false;
            if ((col + dx < 0) || (col + dx > 7))
                return false;
            return CheckEndOfLine(color, dy, dx, row + dy, col + dx, board);
        }

        private void UpdateTurn()
        {
            string actualTurnM = Session["actualTurnM"].ToString();
            if (actualTurnM.Equals("blanco"))
            {
                Session["actualTurnM"] = "negro";
            }
            else if (actualTurnM.ToString().Equals("negro"))
            {
                Session["actualTurnM"] = "blanco";
            }
        }

        private string[,] CleanBoard()
        {
            var gameBoardM = (string[,])Session["gameBoardM"];
            for (int i = 0; i < 8; i++)
                for (int j = 0; j < 8; j++)
                    if (gameBoardM[i, j].Equals("posible"))
                        gameBoardM[i, j] = " ";
            return gameBoardM;
        }

        private void FlipCoins(string color, int row, int col, string[,] board)
        {
            FlipLine(color, -1, -1, row, col, board);
            FlipLine(color, -1, 0, row, col, board);
            FlipLine(color, -1, 1, row, col, board);

            FlipLine(color, 0, -1, row, col, board);
            FlipLine(color, 0, 1, row, col, board);

            FlipLine(color, 1, -1, row, col, board);
            FlipLine(color, 1, 0, row, col, board);
            FlipLine(color, 1, 1, row, col, board);
        }

        private bool FlipLine(string color, int dy, int dx, int row, int col, string[,] board)
        {
            if ((row + dy < 0) || (row + dy > 7))
                return false;
            if ((col + dx < 0) || (col + dx > 7))
                return false;
            if (board[row + dy, col + dx] == " ")
                return false;
            if (board[row + dy, col + dx] == color)
                return true;
            else
            {
                if (FlipLine(color, dy, dx, row + dy, col + dx, board))
                {
                    board[row + dy, col + dx] = color;
                    return true;
                }
                else
                    return false;
            }
        }
        private void CountPoints()
        {
            int contBlack = 0, contWhite = 0;
            var gameBoard = (string[,])Session["gameBoard"];
            for (int i = 0; i < 8; i++)
                for (int j = 0; j < 8; j++)
                {
                    if (gameBoard[i, j] == "negro")
                        contBlack++;
                    else if (gameBoard[i, j] == "blanco")
                        contWhite++;
                }
            Session["whitePoints"] = contWhite;
            Session["blackPoints"] = contBlack;
        }

        private bool TokenPositionValid(int col, int row, string[,] board)
        {
            if (col < 0 || col > 7)
                return false;
            else if (row < 0 || row > 7)
                return false;
            else if (board[row, col] == " ")
                return false;
            else
                return true;
        }

        private void SaveToDB()
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SaveGame";
                var response = (Account)Session["actualUser"];
                cmd.Parameters.Add("@jugador1", SqlDbType.VarChar).Value = response.Username;
                cmd.Parameters.Add("@jugador2", SqlDbType.VarChar).Value = Session["player2"].ToString();
                cmd.Parameters.Add("@puntosN", SqlDbType.Int).Value = Convert.ToInt32(Session["whitePoints"]);
                cmd.Parameters.Add("@puntosB", SqlDbType.Int).Value = Convert.ToInt32(Session["blackPoints"]);
                var bPoints = Convert.ToInt32(Session["whitePoints"]);
                var wPoints = Convert.ToInt32(Session["blackPoints"]);
                string ganador = "ninguno";
                if (wPoints > bPoints)
                    ganador = "Blanco";
                else if (wPoints < bPoints)
                    ganador = "Negro";
                cmd.Parameters.Add("@ganador", SqlDbType.VarChar).Value = ganador;
                cmd.Connection = conn;
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }
    }
}